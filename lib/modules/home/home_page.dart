import 'package:animated_card/animated_card.dart';
import 'package:flutter/material.dart';
import 'package:payflow/modules/extract/extract_page.dart';
import 'package:payflow/modules/home/home_controller.dart';
import 'package:payflow/modules/meus_boletos/meus_boletos_page.dart';
import 'package:payflow/shared/models/user_model.dart';
import 'package:payflow/shared/themes/app_theme.dart';
import 'package:payflow/shared/widgets/boleto_list/boleto_list_controller.dart';

import '../../shared/sizer/sizer.dart';

class HomePage extends StatefulWidget {
  final UserModel user;

  const HomePage({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = HomeController();
  final boletosController = BoletoListController();

  void buscaBoletos() {
    boletosController.getBoletos();
    setState(() {});
    print("################################### buscaBoletos");
  }

  // @override
  // void initState() {
  //   buscaBoletos();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final appBar = PreferredSize(
      preferredSize: Size.fromHeight(152),
      child: Container(
        height: 152,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: AnimatedCard(
            direction: AnimatedCardDirection.top,
            child: ListTile(
              title: Text.rich(
                TextSpan(
                  text: "Olá, ",
                  style: TextStyles.titleRegular,
                  children: [
                    TextSpan(
                      text: "${widget.user.name}",
                      style: TextStyles.titleBoldBackground,
                    ),
                  ],
                ),
              ),
              subtitle: Text(
                "Mantenha suas contas em dia",
                style: TextStyles.captionShape,
              ),
              trailing: Container(
                height: 48,
                width: 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black,
                  image: DecorationImage(
                    image: NetworkImage(widget.user.photoUrl!),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );

    final bottomNavigationBar = Container(
      height: 90,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 25.0.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                onPressed: () {
                  controller.setPage(0);
                  setState(() {});
                },
                icon: Icon(Icons.home,
                    color: controller.currentPage == 0
                        ? Theme.of(context).primaryColor
                        : AppColors.body)),
            Container(
              width: 56,
              height: 56,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(5),
              ),
              child: IconButton(
                  onPressed: () async {
                    await Navigator.pushNamed(context, "/barcode_scanner")
                        .then((value) => value as bool ? buscaBoletos() : null);
                  },
                  icon: Icon(
                    Icons.add_box_outlined,
                    color: Theme.of(context).backgroundColor,
                  )),
            ),
            IconButton(
                onPressed: () {
                  controller.setPage(1);
                  setState(() {});
                },
                icon: Icon(
                  Icons.description_outlined,
                  color: controller.currentPage == 1
                      ? Theme.of(context).primaryColor
                      : AppColors.body,
                )),
          ],
        ),
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: [
        MeusBoletosPage(
          key: UniqueKey(),
        ),
        ExtractPage(
          key: UniqueKey(),
        ),
      ][controller.currentPage],
      backgroundColor: Theme.of(context).backgroundColor,
      bottomNavigationBar: bottomNavigationBar,
    );
  }
}
