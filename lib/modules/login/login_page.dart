import 'package:animated_card/animated_card.dart';
import 'package:flutter/material.dart';
import 'package:payflow/modules/login/login_controller.dart';
import 'package:payflow/shared/themes/app_theme.dart';
import 'package:payflow/shared/widgets/social_login/social_login_button.dart';

import '../../shared/sizer/sizer.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final controller = LoginController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        height: 100.0.h,
        width: 100.0.w,
        child: Stack(
          children: [
            Container(
              color: AppColors.primary,
              width: double.maxFinite,
              height: 35.0.h,
            ),
            Positioned(
              top: 40,
              right: 0,
              left: 0,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    AppImages.person,
                    width: 208,
                    height: 373,
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      width: 208,
                      height: 100,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                            Colors.white.withOpacity(0.0),
                            Colors.white.withOpacity(0.2),
                            Colors.white.withOpacity(0.3),
                            Colors.white.withOpacity(0.4),
                            Colors.white.withOpacity(0.6),
                            Colors.white.withOpacity(0.8),
                            Colors.white.withOpacity(0.9),
                            Colors.white,
                          ])),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 5.0.h,
              left: 0,
              right: 0,
              child: AnimatedCard(
                direction: AnimatedCardDirection.bottom,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      AppImages.logomini,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 30,
                        left: 75,
                        right: 75,
                      ),
                      child: Text(
                        "Organize seus boletos em um só lugar",
                        style: TextStyles.titleHome,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(40),
                      child: SocialLoginButton(
                        onTap: () => controller.googleSignIn(context),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
