import 'package:flutter/material.dart';
import 'package:payflow/shared/themes/app_theme.dart';

class BoletoInfoWidget extends StatelessWidget {
  const BoletoInfoWidget({
    Key? key,
    required this.size,
  }) : super(key: key);

  final int size;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.secondary,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 24),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              AppImages.logomini,
              width: 56,
              height: 34,
              color: Theme.of(context).backgroundColor,
            ),
            Container(
              width: 1,
              height: 32,
              color: Theme.of(context).backgroundColor,
            ),
            Text.rich(
              TextSpan(
                text: "Você tem ",
                style: TextStyles.captionBackground,
                children: [
                  TextSpan(
                    text: "$size boletos\n",
                    style: TextStyles.captionBoldBackground,
                  ),
                  TextSpan(
                    text: "cadastrados para pagar",
                    style: TextStyles.captionBackground,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
