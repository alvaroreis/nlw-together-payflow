import 'package:animated_card/animated_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';

import 'package:payflow/shared/models/boleto_model.dart';
import 'package:payflow/shared/themes/app_theme.dart';

class BoletoTileWidget extends StatelessWidget {
  const BoletoTileWidget({
    Key? key,
    required this.data,
  }) : super(key: key);

  final BoletoModel data;

  @override
  Widget build(BuildContext context) {
    final _valor = MoneyMaskedTextController(
      initialValue: data.value!,
      precision: 2,
      decimalSeparator: ",",
      thousandSeparator: ".",
    );

    return AnimatedCard(
      direction: AnimatedCardDirection.right,
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(data.name!, style: TextStyles.titleListTile),
        subtitle: Text(
          "Vence em ${data.dueDate!}",
          style: TextStyles.captionBody,
        ),
        trailing: Text.rich(
          TextSpan(
            text: "R\$ ",
            style: TextStyles.trailingRegular,
            children: [
              TextSpan(
                text: _valor.text,
                style: TextStyles.trailingBold,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
