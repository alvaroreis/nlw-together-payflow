import 'package:flutter/material.dart';
import 'package:payflow/shared/themes/app_theme.dart';

class DividerVerticalWidget extends StatelessWidget {
  const DividerVerticalWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1,
      color: AppColors.stroke,
    );
  }
}
