import 'package:flutter/material.dart';
import 'package:payflow/shared/themes/app_theme.dart';

class SocialLoginButton extends StatelessWidget {
  const SocialLoginButton({Key? key, required this.onTap}) : super(key: key);

  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      // splashColor: ,
      child: Ink(
        height: 56,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: AppColors.shape,
            border: Border.fromBorderSide(BorderSide(color: AppColors.stroke))),
        child: Row(
          children: [
            Expanded(
              flex: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(AppImages.google),
                  SizedBox(
                    width: 20,
                  ),
                  // Container(
                  //   height: 56,
                  //   width: 1,
                  //   color: AppColors.stroke,
                  // )
                  Text(
                    "Entrar com o Google",
                    style: TextStyles.buttonGray,
                  ),
                ],
              ),
            ),
            // Expanded(
            //   flex: 70,
            //   child: Text(
            //     "Entrar com o Google",
            //     style: TextStyles.buttonGray,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
